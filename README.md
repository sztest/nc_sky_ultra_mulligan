*This mod is an add-on for [Nodecore Skyblock Ultra Hardcore](/packages/Kimapr/nc_sky_ultra_hard/)*

If you fall off of your island, under most circumstances, you will start over on a fresh island and lose all progress, unless you manage to bridge back to your original island.  This can happen for unfair reasons, like being pushed out of a node, or network lag prediction glitches. This mod makes it possible to construct an Antipyre which will save you from starting over.

**To construct one**, plant 5 trees in a + shape underneath an open sky.  The outer 4 need to be grown to their first stage (a growing tree trunk atop a stump) and the center one to its second stage (growing tree trunk, atop a tree trunk, atop a stump).  Then plant eggcorn sprouts on top of all 5, and pummel the center one with another eggcorn.

Once you have an antipyre, if you fall off of your island, you will be teleported to the top of that antipyre.

The antipyre is destroyed if it is disturbed in any way, including being used to save you from a fall, being dug, having its supporting platform dug out from under it, having anything fall and rest on it, or having its view of the sky obstructed.  If it's broken or consumed, it will burst into flames.

Each player can only have one antipyre at a time, though they can be replaced if destroyed for any reason.  An antipyre can be destroyed by other players, but the craft cannot be completed on another player's behalf.