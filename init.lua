-- LUALOCALS < ---------------------------------------------------------
local ipairs, math, minetest, nc_sky_ultra_hard, nodecore, pairs,
      rawset, string, vector
    = ipairs, math, minetest, nc_sky_ultra_hard, nodecore, pairs,
      rawset, string, vector
local math_random, string_format
    = math.random, string.format
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local api = {}
rawset(_G, modname, api)

local hashpos = minetest.hash_node_position

do
	local metakey = "db"

	local modstore = minetest.get_mod_storage()
	local db = modstore:get_string(metakey)
	db = db and db ~= "" and minetest.deserialize(db, true) or {}

	local pending

	function api.get(pname)
		return db[pname]
	end
	function api.set(pname, ent)
		db[pname] = ent
		if not pending then
			pending = true
			return minetest.after(0, function()
					modstore:set_string(metakey, minetest.serialize(db))
					pending = nil
				end)
		end
	end
	function api.allkeys()
		local t = {}
		for k in pairs(db) do t[#t + 1] = k end
		return t
	end
end

local igniting = {}
local function altarignite(pos)
	local key = hashpos(pos)
	if not igniting[key] then
		igniting[key] = true
		nodecore.fire_ignite(pos)
		igniting[key] = nil
	end
	minetest.after(0, function()
			for _, p in ipairs(nodecore.find_nodes_around(pos,
			"group:flammable")) do
				nodecore.fire_ignite(p)
			end
		end)
end

local mydesc = "Antipyre"
minetest.register_node(modname .. ":altar", {
		description = mydesc,
		drawtype = "mesh",
		visual_scale = nodecore.z_fight_ratio,
		mesh = "nc_api_storebox_box.obj",
		backface_culling = true,
		use_texture_alpha = "clip",
		tiles = {"nc_woodwork_plank.png^[multiply:#804060:255"
			.. "^nc_api_loose.png^[mask:nc_lode_shelf_base.png"},
		groups = {
			[modname] = 1,
			snappy = 1,
			flammable = 1,
			fire_fuel = 3,
			falling_node = 1
		},
		paramtype = "light",
		drop = "",
		on_dig = altarignite,
		on_ignite = altarignite,
		falling_replacement = "nc_fire:ember3",
		on_node_touchtip = function(pos)
			local owner = minetest.get_meta(pos):get_string("player")
			if owner and owner ~= "" then
				return mydesc .. "\n" .. nodecore.notranslate(owner)
			end
		end,
		sounds = nodecore.sounds("nc_tree_woody"),
		mapcolor = {r = 128, g = 52, b = 52, a = 160},
	})

nodecore.register_falling_node_on_setnode(function(self, node)
		if node and node.name and minetest.get_item_group(node.name, modname) > 0 then
			self.node.name = "nc_fire:ember3"
		end
	end)

local recipenodes = {
	{match = "nc_tree:eggcorn_planted"},
	{y = -1, match = "nc_tree:tree_bud"},
	{y = -1, x = 1, match = "nc_tree:eggcorn_planted"},
	{y = -1, x = -1, match = "nc_tree:eggcorn_planted"},
	{y = -1, z = 1, match = "nc_tree:eggcorn_planted"},
	{y = -1, z = -1, match = "nc_tree:eggcorn_planted"},
	{y = -2, match = "nc_tree:tree"},
	{y = -2, x = 1, match = "nc_tree:tree_bud"},
	{y = -2, x = -1, match = "nc_tree:tree_bud"},
	{y = -2, z = 1, match = "nc_tree:tree_bud"},
	{y = -2, z = -1, match = "nc_tree:tree_bud"},
	{y = -3, match = "nc_tree:root"},
	{y = -3, x = 1, match = "nc_tree:root"},
	{y = -3, x = -1, match = "nc_tree:root"},
	{y = -3, z = 1, match = "nc_tree:root"},
	{y = -3, z = -1, match = "nc_tree:root"}
}
local altarnodes = {}
local skynodes = {}
do
	for _, v in ipairs(recipenodes) do
		v.x = v.x or 0
		v.y = v.y or 0
		v.z = v.z or 0
		if v.match == "nc_tree:eggcorn_planted" then
			v.replace = "air"
			skynodes[#skynodes + 1] = v
		else
			v.replace = modname .. ":altar"
			altarnodes[#altarnodes + 1] = v
		end
	end
end

nodecore.register_craft({
		label = "make respawn altar",
		action = "pummel",
		duration = 4,
		wield = {name = "nc_tree:eggcorn", count = false},
		wieldconsume = 1,
		priority = 100,
		check = function(pos, data)
			local pname = data.crafter and data.crafter:is_player()
			and data.crafter:get_player_name()
			if (not pname) or api.get(pname) then return end
			for _, p in ipairs(skynodes) do
				p = vector.add(pos, p)
				p.y = p.y + 1
				if not nodecore.is_full_sun(p)
				then return end
			end
			return true
		end,
		nodes = recipenodes,
		after = function(pos, data)
			local pname = data.crafter:get_player_name()
			for _, p in ipairs(altarnodes) do
				local meta = minetest.get_meta(vector.add(pos, p))
				meta:set_string("player", pname)
				meta:set_string("rel", minetest.pos_to_string(p))
			end
			api.set(pname, pos)
		end
	})

do
	local queue = {}
	local nextrecycle = 0
	minetest.register_globalstep(function()
			if #queue < 1 then
				if nodecore.gametime < nextrecycle then return end
				nextrecycle = nodecore.gametime + 1 + math_random()
				queue = api.allkeys()
				if #queue < 1 then return end
			end

			local pname = queue[#queue]
			queue[#queue] = nil

			local pos = api.get(pname)
			if not pos then return end

			local function fail(reason, pp)
				nodecore.log("action", string_format(
						"%s respawn alter breaks because %s at %s",
						pname, reason, minetest.pos_to_string(pp)))
				return api.set(pname, nil)
			end
			for _, p in ipairs(altarnodes) do
				local pp = vector.add(pos, p)

				local node = minetest.get_node_or_nil(pp)
				if node then
					if minetest.get_item_group(node.name, modname) <= 0
					then return fail("missing node", pp) end

					local meta = minetest.get_meta(pp)
					if meta:get_string("player") ~= pname
					or meta:get_string("rel") ~= minetest.pos_to_string(p)
					then return fail("wrong metadata", pp) end
				end
			end
			for _, p in ipairs(skynodes) do
				local pp = vector.add(pos, p)
				if nodecore.get_node_light(pp) and not nodecore.is_full_sun(pp)
				then return fail("blocked skylight", pp) end
				for _ = 1, 3 do
					if not minetest.get_node_or_nil(pp) then break end
					if not nodecore.air_equivalent(pp) then
						return fail("covered above", pp)
					end
					pp.y = pp.y + 1
				end
			end
		end)
end

minetest.register_abm({
		label = "altar validation",
		nodenames = {"group:" .. modname},
		interval = 1,
		chance = 1,
		action = function(pos)
			local meta = minetest.get_meta(pos)
			local pname = meta:get_string("player")
			local abspos = pname and api.get(pname)
			if not abspos then return altarignite(pos) end
			local rel = meta:get_string("rel")
			rel = rel and rel ~= "" and minetest.string_to_pos(rel)
			if (not rel) or not vector.equals(vector.add(abspos, rel), pos)
			then return altarignite(pos) end
		end
	})

local oldttl = nc_sky_ultra_hard.get_island_ttl
function nc_sky_ultra_hard.get_island_ttl(name, ...)
	if api.get(name) then return nc_sky_ultra_hard.assign_ttl end
	return oldttl(name, ...)
end

local oldsend = nc_sky_ultra_hard.send_to_island
function nc_sky_ultra_hard.send_to_island(player, ...)
	local pname = player:get_player_name()
	local apos = api.get(pname)
	if apos then
		player:set_pos(apos)
		nodecore.log("action", pname .. " consumes respawn altar")
		return api.set(pname)
	end
	return oldsend(player, ...)
end
